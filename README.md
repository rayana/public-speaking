# public-speaking

A collection of my presentations and other open-source stuff related to public speaking.

# Speaking timeline

## 2020

* [We can embrace the future of work right now](https://gitlab.com/rayana/public-speaking/-/tree/master/talks%2F2020-toptal-future-of-work) @ Toptal Future of Work Series - Amsterdam

## 2019

* [Re-designing A Life You Love](https://womentalkdesign.com/talks/re-designing-a-life-you-love/) @ Ladies that UX Amsterdam

## 2018

* [Bridging The Gap — Produce and Communicate Design Decisions to Developers](https://speakerdeck.com/rayana/bridging-the-gap-produce-and-communicate-design-decisions-to-developers) @ Ladies that UX Amsterdam
* [Empowering Product Growth With A Design System](https://speakerdeck.com/rayana/empowering-product-growth-with-a-design-system) @ Ladies that UX Amsterdam
* Empowering Product Growth With A Design System @ Backbase Design Day

# Speaker Bio

Rayana Verissimo is a Brazilian designer living in The Netherlands. She is a Senior Product Designer at GitLab, an open source collaboration tool for software development used by more than 100,000 organizations around the globe. In past lives, Rayana led design systems at Backbase, and did user interface engineering work for Apple.

With a background in Graphic Design, her strength lies in UI Design and Engineering. Rayana cares about thoughtfully creating collaborative design and empowering teams to build successful experiences in a systematic way, by working at the intersection of design and development.

# Get in touch

Want me to speak about something? Get in touch with me :)

* Gmail rayanaverissimo
* GitLab [@rayana](https://gitlab.com/rayana)
* Twitter [@imrayana](https://twitter.com/imrayana)
* LinkedIn [rayanaverissimo](https://www.linkedin.com/in/rayanaverissimo/)
