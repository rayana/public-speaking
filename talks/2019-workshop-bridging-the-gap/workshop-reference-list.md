# Workshop - reference list

This file lists the online references I used to build my workshop "Bridging The Gap — Produce and Communicate Design Decisions to Developers".

Slides can be found here: https://speakerdeck.com/rayana/bridging-the-gap-produce-and-communicate-design-decisions-to-developers

## Resources

* What Does My Site Cost? Find out how much it costs for someone to use your site on mobile networks around the world. - https://whatdoesmysitecost.com
* HTML Basics - Mozilla docs - https://developer.mozilla.org/en-US/docs/Learn/Getting_started_with_the_web/HTML_basics
* Learning HTML: Guides and tutorials - https://developer.mozilla.org/en-US/docs/Learn/HTML
* Everything you need to know about this CSS preprocessor - https://www.creativebloq.com/web-design/what-is-sass-111517618
* How the Web works - https://developer.mozilla.org/en-US/docs/Learn/Getting_started_with_the_web/How_the_Web_works
* W3Schools - https://www.w3schools.com
* Introduction to the CSS basic box model - https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Box_Model/Introduction_to_the_CSS_box_model

## Design - Developer collaboration

* Keys to effective designer-developer collaboration - https://insights.dice.com/2017/09/22/keys-effective-designer-developer-collaboration/
* Working Together: How Designers And Developers Can Communicate To Create Better Projects - https://www.smashingmagazine.com/2018/04/working-together-designers-developers/
* Bridging UX & Web Development: Better Results Through Team Integration - https://www.uxmatters.com/mt/archives/2014/10/bridging-ux-web-development-better-results-through-team-integration.php
* The overlap between design and engineering: a different perspective on product development - https://cheesecakelabs.com/blog/overlap-between-design-engineering/
* Desgning with developers in mind: Guides, grids and frameworks - https://www.invisionapp.com/blog/design-with-developers-in-mind/
* How To Effectively Communicate With Developers - https://www.smashingmagazine.com/2009/08/how-to-effectively-communicate-with-developers/
* Designers. Work better with developers. - https://medium.com/swlh/designers-work-better-with-developers-ecd509a00d30
* Integrating UX design into a DSDM project - http://www.id-book.com/casestudy_12-1_2.php

## Wireframing and Prototyping

* Which UX Deliverables Are Most Commonly Created and Shared? - https://www.nngroup.com/articles/common-ux-deliverables/
* The Right Tool For The Job: Picking The Best Prototyping Software For Your Project - https://www.smashingmagazine.com/2016/06/picking-the-best-prototyping-software-for-your-project/
* Wireframing for responsive design - https://boagworld.com/design/wireframing-for-responsive-design/
* UX docs and deliverables - http://www.uxforthemasses.com/resources/example-ux-docs/

## UI Components

* 6 Reasons for Employing Component-based UI Development - https://www.tandemseven.com/technology/6-reasons-component-based-ui-development/
* UI component playbook - https://blog.hichroma.com/ui-component-playbook-fd3022d00590
* From Pages to Patterns: An Exercise for Everyone - http://alistapart.com/article/from-pages-to-patterns-an-exercise-for-everyone
* Designing the Steps in the User Interface for a Difficult Task - https://blog.learningtree.com/designing-steps-user-interface-difficult-task/
* Component cut-up workshop - https://medium.com/eightshapes-llc/the-component-cut-up-workshop-1378ae110517
* Modular Web Design With Components - https://speakerdeck.com/nadalsol/modular-web-design-with-components
* Tips & tricks for creating reusable UI components (dev) - https://medium.freecodecamp.org/tips-tricks-for-creating-reusable-ui-components-2b1452147bda

## Accessibility

* Don’t Use The Placeholder Attribute - https://www.smashingmagazine.com/2018/06/placeholder-attribute/
* A Comprehensive Guide to Styling File Inputs - https://developer.telerik.com/featured/comprehensive-guide-styling-file-inputs/

## Design Sytems

* Atomic Design - http://atomicdesign.bradfrost.com/table-of-contents/
* This is the web - http://bradfrost.com/blog/post/this-is-the-web/

## Testing

* Component QA in Design Systems - https://medium.com/eightshapes-llc/component-qa-in-design-systems-b18cb4decb9c
* How to Test Visual Design - https://www.nngroup.com/articles/testing-visual-design/

## Books

* Designing Interactions - http://www.designinginteractions.com
* The Unusually Useful Web Book - https://books.google.nl/books?id=extn_UeicFoC&printsec=frontcover&source=gbs_ge_summary_r&cad=0#v=onepage&q&f=false